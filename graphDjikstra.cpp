// Alex Bilozertchev
// Compile with: "g++ -std=c++11 graphDjikstra.cpp -o graphDjikstra"

// Libraries
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <vector>
#include <numeric>
#include <ctime>

// This is the definition for part B: the number of new edges to add.
#define INSERTIONS 100

// Standard namespace
using namespace std;

// Vector responsible for holding the generated shortest path values
vector <double> paths;

// Implemented, modified shortest path algorithm:

// A structure to represent a node in adjacency list
struct AdjListNode
{
    int dest;
    double weight;
    struct AdjListNode* next;
};

// A structure to represent an adjacency liat
struct AdjList
{
    struct AdjListNode *head;  // pointer to head node of list
};

// A structure to represent a graph. A graph is an array of adjacency lists.
// Size of array will be V (number of vertices in graph)
struct Graph
{
    int V;
    struct AdjList* array;
};

// A utility function to create a new adjacency list node
struct AdjListNode* newAdjListNode(int dest, double weight)
{
    struct AdjListNode* newNode =
    (struct AdjListNode*) malloc(sizeof(struct AdjListNode));
    newNode->dest = dest;
    newNode->weight = weight;
    newNode->next = NULL;
    return newNode;
}

// A utility function that creates a graph of V vertices
struct Graph* createGraph(int V)
{
    struct Graph* graph = (struct Graph*) malloc(sizeof(struct Graph));
    graph->V = V;
    
    // Create an array of adjacency lists.  Size of array will be V
    graph->array = (struct AdjList*) malloc(V * sizeof(struct AdjList));
    
    // Initialize each adjacency list as empty by making head as NULL
    for (int i = 0; i < V; ++i)
        graph->array[i].head = NULL;
    
    return graph;
}

// Adds an edge to an undirected graph
void addEdge(struct Graph* graph, int src, int dest, double weight)
{
    // Add an edge from src to dest.  A new node is added to the adjacency
    // list of src.  The node is added at the begining
    struct AdjListNode* newNode = newAdjListNode(dest, weight);
    newNode->next = graph->array[src].head;
    graph->array[src].head = newNode;
    
    // Since graph is undirected, add an edge from dest to src also
    newNode = newAdjListNode(src, weight);
    newNode->next = graph->array[dest].head;
    graph->array[dest].head = newNode;
}

// Structure to represent a min heap node
struct MinHeapNode
{
    int  v;
    double dist;
};

// Structure to represent a min heap
struct MinHeap
{
    int size;      // Number of heap nodes present currently
    int capacity;  // Capacity of min heap
    int *pos;     // This is needed for decreaseKey()
    struct MinHeapNode **array;
};

// A utility function to create a new Min Heap Node
struct MinHeapNode* newMinHeapNode(int v, int dist)
{
    struct MinHeapNode* minHeapNode =
    (struct MinHeapNode*) malloc(sizeof(struct MinHeapNode));
    minHeapNode->v = v;
    minHeapNode->dist = dist;
    return minHeapNode;
}

// A utility function to create a Min Heap
struct MinHeap* createMinHeap(int capacity)
{
    struct MinHeap* minHeap =
    (struct MinHeap*) malloc(sizeof(struct MinHeap));
    minHeap->pos = (int *)malloc(capacity * sizeof(int));
    minHeap->size = 0;
    minHeap->capacity = capacity;
    minHeap->array =
    (struct MinHeapNode**) malloc(capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}

// A utility function to swap two nodes of min heap. Needed for min heapify
void swapMinHeapNode(struct MinHeapNode** a, struct MinHeapNode** b)
{
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}

// A standard function to heapify at given idx
// This function also updates position of nodes when they are swapped.
// Position is needed for decreaseKey()
void minHeapify(struct MinHeap* minHeap, int idx)
{
    int smallest, left, right;
    smallest = idx;
    left = 2 * idx + 1;
    right = 2 * idx + 2;
    
    if (left < minHeap->size &&
        minHeap->array[left]->dist < minHeap->array[smallest]->dist )
        smallest = left;
    
    if (right < minHeap->size &&
        minHeap->array[right]->dist < minHeap->array[smallest]->dist )
        smallest = right;
    
    if (smallest != idx)
    {
        // The nodes to be swapped in min heap
        MinHeapNode *smallestNode = minHeap->array[smallest];
        MinHeapNode *idxNode = minHeap->array[idx];
        
        // Swap positions
        minHeap->pos[smallestNode->v] = idx;
        minHeap->pos[idxNode->v] = smallest;
        
        // Swap nodes
        swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
        
        minHeapify(minHeap, smallest);
    }
}

// A utility function to check if the given minHeap is ampty or not
int isEmpty(struct MinHeap* minHeap)
{
    return minHeap->size == 0;
}

// Standard function to extract minimum node from heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
{
    if (isEmpty(minHeap))
        return NULL;
    
    // Store the root node
    struct MinHeapNode* root = minHeap->array[0];
    
    // Replace root node with last node
    struct MinHeapNode* lastNode = minHeap->array[minHeap->size - 1];
    minHeap->array[0] = lastNode;
    
    // Update position of last node
    minHeap->pos[root->v] = minHeap->size-1;
    minHeap->pos[lastNode->v] = 0;
    
    // Reduce heap size and heapify root
    --minHeap->size;
    minHeapify(minHeap, 0);
    
    return root;
}

// Function to decreasy dist value of a given vertex v. This function
// uses pos[] of min heap to get the current index of node in min heap
void decreaseKey(struct MinHeap* minHeap, int v, int dist)
{
    // Get the index of v in  heap array
    int i = minHeap->pos[v];
    
    // Get the node and update its dist value
    minHeap->array[i]->dist = dist;
    
    // Travel up while the complete tree is not hepified.
    // This is a O(Logn) loop
    while (i && minHeap->array[i]->dist < minHeap->array[(i - 1) / 2]->dist)
    {
        // Swap this node with its parent
        minHeap->pos[minHeap->array[i]->v] = (i-1)/2;
        minHeap->pos[minHeap->array[(i-1)/2]->v] = i;
        swapMinHeapNode(&minHeap->array[i],  &minHeap->array[(i - 1) / 2]);
        
        // move to parent index
        i = (i - 1) / 2;
    }
}

// A utility function to check if a given vertex
// 'v' is in min heap or not
bool isInMinHeap(struct MinHeap *minHeap, int v)
{
    if (minHeap->pos[v] < minHeap->size)
        return true;
    return false;
}

// A utility function used to print the solution
void printArr(double dist[], int n, int src)
{
    //printf("Vertex   Distance from Source\n");
    for (int i = src; i < n; ++i) {
        
        // Print to screen
        //printf("%d \t\t %f\n", i, dist[i]);
        
        // Add to vector
        paths.push_back(dist[i]);
    }
}

// The main function that calulates distances of shortest paths from src to all
// vertices. It is a O(ELogV) function
void dijkstra(struct Graph* graph, int src)
{
    int V = graph->V;// Get the number of vertices in graph
    double dist[V];      // dist values used to pick minimum weight edge in cut
    
    // minHeap represents set E
    struct MinHeap* minHeap = createMinHeap(V);
    
    // Initialize min heap with all vertices. dist value of all vertices
    for (int v = 0; v < V; ++v)
    {
        dist[v] = INT_MAX;
        minHeap->array[v] = newMinHeapNode(v, dist[v]);
        minHeap->pos[v] = v;
    }
    
    // Make dist value of src vertex as 0 so that it is extracted first
    minHeap->array[src] = newMinHeapNode(src, dist[src]);
    minHeap->pos[src]   = src;
    dist[src] = 0;
    decreaseKey(minHeap, src, dist[src]);
    
    // Initially size of min heap is equal to V
    minHeap->size = V;
    
    // In the followin loop, min heap contains all nodes
    // whose shortest distance is not yet finalized.
    while (!isEmpty(minHeap))
    {
        // Extract the vertex with minimum distance value
        struct MinHeapNode* minHeapNode = extractMin(minHeap);
        int u = minHeapNode->v; // Store the extracted vertex number
        
        // Traverse through all adjacent vertices of u (the extracted
        // vertex) and update their distance values
        struct AdjListNode* pCrawl = graph->array[u].head;
        while (pCrawl != NULL)
        {
            int v = pCrawl->dest;
            
            // If shortest distance to v is not finalized yet, and distance to v
            // through u is less than its previously calculated distance
            if (isInMinHeap(minHeap, v) && dist[u] != INT_MAX &&
                pCrawl->weight + dist[u] < dist[v])
            {
                dist[v] = dist[u] + pCrawl->weight;
                
                // update distance value in min heap also
                decreaseKey(minHeap, v, dist[v]);
            }
            pCrawl = pCrawl->next;
        }
    }
    
    // print the calculated shortest distances
    printArr(dist, V, src);
}


// Driver program to test above functions
int main(int argc, const char * argv[]) {
    
    // Two graphs, to perform simultaneous calculations
    struct Graph* graph = NULL;
    struct Graph* graph2 = NULL;

    // Variables required for reading data and tracking program logic
    int start = 0, count = 0, threshold = 0, V = 0;
    string line;
    string filename;
    
    cout << "Dijkstra's shortest path v0.00001" << endl;
    cout << "Enter filename: ";
    cin >> filename;
    
    // Read in the data
    ifstream myfile (filename);
    if (myfile.is_open())
    {   
        // Read in each line
        while ( getline (myfile,line) )
        {
            if (line[0] == '%') {
                // Skip the comments and continue
                continue;
            }
            else {
                // Separate getline into ints and a double
                istringstream iss(line);
                int sub1, sub2;
                double sub3;
                
                // Grab the header line and read the first number into graph size + create graph
                if (V == 0) {
                    iss >> sub1;
                    V = ++sub1;
                    graph = createGraph(V);
                    graph2 = createGraph(V+INSERTIONS);
                    cout << "Graph size detected: " << --V << endl << endl;
                }
                
                // Add the edges and weights
                else {
                    iss >> sub1;
                    iss >> sub2;
                    iss >> sub3;
                    
                    // Add the edges to each graph for computations
                    addEdge(graph, sub1, sub2, sub3);
                    addEdge(graph2, sub1, sub2, sub3);
                    
                }
            }
        }

        // Close the file
        myfile.close();
    }
    
    // Filename error
    else {
        cout << "Unable to open file: " << filename << endl;
        exit(0);
    }

    // Ask for starting vertex
    cout << "Starting vertex: ";
    cin >> start;
    cout << "Threshold: ";
    cin >> threshold;
    
    // Timing code
    clock_t begin1 = clock();

        // Run algorithm
        dijkstra(graph, start);
    
    clock_t end1 = clock();

    double elapsed_secs = double(end1 - begin1) / CLOCKS_PER_SEC;
    
    // Analysis output for running the algorithm on initial test data:
    cout << endl << "--- ANALYSIS ---" << endl;
    
    auto max = max_element(begin(paths), end(paths));   // Finds max element
    auto min = min_element(begin(paths), end(paths));   // Finds min element
    if (*min == 0) { min++; }   // Min element assumed to not be zero weight
    double avg = accumulate(paths.begin(), paths.end(), 0.0)/paths.size();  // Finds the average
    
    // Various output to user:
    cout << "Computation time: " << elapsed_secs << " seconds" << endl;
    cout << "Maximum Edge Weight: " << *max << endl;
    cout << "Minimum Edge Weight: " << *min << endl;
    cout << "Average Edge Weight: " << avg << endl;
    cout << endl;
    
    cout << "Using threshold = " << threshold << ":" << endl;
    cout << "Length (x)" << "\tOccurences (y)" << endl;
    
    // Threshold calculation
    for(count = start; count < threshold; count++) {
        cout << count << "\t\t\t" << std::count(paths.begin(), paths.end(), count) << endl; // Count the number of times a path quantity appears
    }

    // Check for zero threshold
    if (threshold == 0) {
        cout << "N/A, threshold = 0" << endl;
    }
    cout << endl;
    
    // Generate wolfram plot to easily copy/paste for plotting
    for(count = start; count < threshold; count++) {
        if (count == start) {
            cout << "Wolfram Plot Function: " << endl;
            cout << "plot[";
        }
        
        // Place all x,y values in proper format
        cout << "{" << count << "," << std::count(paths.begin(), paths.end(), count) << "}";
        
        if (++count != threshold) {
            cout << ",";
            --count;
        }
        else {
            cout << "]" << endl << endl;
        }
    }

    // Functionality to add an edge:
    int v_start, v_end;
    double v_weight;

    // Timing code
    clock_t begin = clock();

        // Code to add a specific number of new edges to the graph
        for(int i=0;i<INSERTIONS;++i)
        {
            v_start = (rand()%(300+i))+1;
            v_end = 301+i;
            v_weight = rand()%10;
            addEdge(graph2, v_start, v_end, v_weight);
            
            // Troubleshooting printouts... (These are removed during timing analysis to save cpu cycles)
            cout << "Adding edge from " << v_start << " to " << v_end << " with a weight of " << v_weight;
            cout << "\tShortest distance is to " << v_end << " is " << paths[v_start-1] + v_weight << endl;
        }

    clock_t end = clock();
    elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

    // Analysis output for inserting the new edges WITHOUT recomputing graph:
    cout << endl << "--- ANALYSIS FOR INSERTING " << INSERTIONS << " EDGES---" << endl;
    cout << "Computation time: " << elapsed_secs << " seconds" << endl;

        // Timing code
    clock_t begin2 = clock();

        // Run algorithm
        dijkstra(graph2, start);
    
    clock_t end2 = clock();
    elapsed_secs = double(end2 - begin2) / CLOCKS_PER_SEC;

    // Analysis output for inserting the new edges AND recomputing graph:
    cout << endl << "--- ANALYSIS RECOMPUTING " << 300 + INSERTIONS << " EDGES ---" << endl;
    cout << "Computation time: " << elapsed_secs << " seconds" << endl;

    return 0;
}











